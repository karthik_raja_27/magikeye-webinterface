from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, Http404
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterationForm
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage

from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

import json, requests, ast, os, mimetypes, base64
from pathlib import Path
from connecto import views as app_view


# Application views here.
def register(request):
    if request.method == 'POST':
        form = UserRegisterationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Your account has been created! Please log in!')
            return redirect('login')
    else:
        form = UserRegisterationForm()
    return render(request, 'register.html', {'form': form})

@login_required()
def profile(request):
    return render(request, 'profile.html')


def create_datasets(request):
    if request.method == 'POST':
        msg = None

        username = request.user.username
        payload = {'username': username}
        payload = json.dumps(payload)

        """
        if username not in data_storage.list_users():
            data_storage.create_user(username)
        """
        response = requests.get(app_view.DB_HOST + app_view.getFSUsers_api)
        fs_users = (response.json())["users"]
        if username not in fs_users:
            payload = {"username": username}
            payload = json.dumps(payload)
            requests.post(app_view.DB_HOST + app_view.createFSUser_api, data=payload)

        # DB call :: createDataSet
        response = requests.post(app_view.DB_HOST + app_view.create_data_set_api, data=payload)
        data_set_id = int(response.json()['data_set_id'])

        """
            data_storage.create_dataset(username, data_set_id)
        """
        payload = {"username": username, "data_set_id": data_set_id}
        payload = json.dumps(payload)
        requests.post(app_view.DB_HOST + app_view.createFSDataset_api, data=payload)

        if 'dataset' in request.FILES:
            # Create dataset along with the Datafile!
            file_uploaded = request.FILES['dataset']
            file_name = file_uploaded.name

            payload = {"username": username, "data_set_id": data_set_id, "filename": file_name}
            requests.post(app_view.DB_HOST + app_view.addFileToFSDataset_api, data=payload, files={'file': file_uploaded})

            msg = f"Your Dataset-{data_set_id} has been Created with file {file_uploaded.name} Successfully!"

        if not msg: msg = f"Your Dataset-{data_set_id} has been Created Successfully!"
        messages.success(request, msg)

    return redirect('home')

def upload_files_for_DS(request):
    if request.method == 'POST':
        username = request.user.username

        file_uploaded = request.FILES['datasetfile']
        data_set_id = request.POST['data_set_id']
        file_name = file_uploaded.name

        """data_storage.add_file_to_dataset(username, data_set_id, f.name)"""
        payload = {"username": username, "data_set_id": data_set_id, "filename": file_name}
        requests.post(app_view.DB_HOST + app_view.addFileToFSDataset_api, data=payload, files={'file': file_uploaded})

        messages.success(request, f'Your File {file_uploaded.name} has been uploaded to the Dataset-{data_set_id} Successfully!')

    return redirect('home')

def upload_files_for_DS_from_DSD(request):
    if request.method == 'POST':
        username = request.user.username

        file_uploaded = request.FILES['datasetfile']
        data_set_id = request.POST['data_set_id']
        file_name = file_uploaded.name

        payload = {"username": username, "data_set_id": data_set_id, "filename": file_name}
        requests.post(app_view.DB_HOST + app_view.addFileToFSDataset_api, data=payload, files={'file': file_uploaded})

        messages.success(request, f'Your File {file_uploaded.name} has been uploaded to the Dataset-{data_set_id} Successfully!')

    return redirect('dataSetDetails', data_set_id=data_set_id)

def update_comment_for_DS(request):
    if request.method == 'POST':
        comment = request.POST['datasetcomment']
        data_set_id = request.POST['data_set_id']
        payload = {'data_set_id': data_set_id, 'comment': comment}
        payload = json.dumps(payload)

        # DB call :: updateDataSet
        response = requests.post(app_view.DB_HOST + app_view.update_data_set_api, data=payload)
        if response.status_code == 200:
            messages.success(request, f'Your comment has been updated to the Dataset-{data_set_id} Successfully!')
        else:
            messages.warning(request, f"Your comment couldn't be updated to the Dataset-{data_set_id}, Please try again!")
    return redirect('home')

def remove_dataset(request):
    if request.method == 'POST':
        username = request.user.username

        data_set_id = request.POST['data_set_id']
        payload = {'data_set_id': data_set_id}
        payload = json.dumps(payload)

        # DB call :: removeDataSet
        response = requests.post(app_view.DB_HOST + app_view.remove_data_set_api, data=payload)
        if response.status_code == 200:
            messages.success(request, f'Your Dataset-{data_set_id} has been removed Successfully!')
            payload = {'data_set_id': data_set_id}
            payload = json.dumps(payload)
            # FS call :: removeFSDataset
            response = requests.post(app_view.DB_HOST + app_view.removeFSDataset_api, data=payload)
            if response.status_code == 200:
                messages.success(request, f'Your Dataset-{data_set_id} has been removed\
                                            from both DB and FS Successfully')
            else:
                messages.warning(request, f"Your Dataset-{data_set_id} has been removed\
                                            from DB Successfully, but still exists in filesystem!")

        else:
            messages.warning(request, f"Your Dataset-{data_set_id} couldn't be removed , Please try again!")

    return redirect('home')


def create_job(request):
    if request.method == 'POST':
        username = request.user.username
        file_uploaded = request.FILES['jobfile']

        # Job File Validation - 1
        file_extension_validated_successfully = True
        acceptable_file_formats = ['json']

        file_extension = (file_uploaded.name.split('.'))[-1]
        if file_extension.lower().strip() not in acceptable_file_formats:
            file_extension_validated_successfully = False
            msg = f"File extension - '{file_extension.lower().strip()}' is not acceptable file formats: {acceptable_file_formats}"
            messages.warning(request, f"Cannot validate job JSON: {msg}")

        if file_extension_validated_successfully:
            json_file_content = file_uploaded.read()
            job_file_as_dict = ast.literal_eval(json_file_content.decode('utf-8'))
            j_type = job_file_as_dict['type']
            dataset_id = job_file_as_dict['dataset_id']

            # DB call :: createJob
            payload = {'username': username, 'jobs_type': j_type, 'jobs_process': 'PROCESS_UNKNOWN',
                       'dataset_id': dataset_id}
            payload = json.dumps(payload)
            # payload should needs to be updated with the process!

            response = requests.post(app_view.DB_HOST + app_view.create_job_api, data=payload)
            job_id = int(response.json()['job_id'])

            # Job File Validation - 2
            job_file_validated_successfully = True

            try:
                job_type, job_json = job_executor.validate_job(username, job_json=job_file_as_dict)
            except Exception as msg:
                job_file_validated_successfully = False
                messages.warning(request, f"Cannot validate job JSON: {str(msg)}")

            if job_file_validated_successfully:
                # Execute job [#create job in data_storage is taken care by the job_executor]
                pid = job_executor.execute_job(username, job_id, job_file_as_dict)

                pstatus = job_executor.PROCESS_UNKNOWN

                while pstatus >= 0:
                    pstatus, errmsg = job_executor.query_process(pid)

                if pstatus == job_executor.PROCESS_SUCCESS:
                    pstatus  = f"Process successful: {pid}"
                elif pstatus == job_executor.PROCESS_FAILURE:
                    pstatus  = f"Process failure: {pid}, {errmsg}"
                else:
                    pstatus = f"Process running: {pid}"


                # DB call :: updateJob
                payload = {'job_id': job_id,  'jobs_process': pstatus}
                payload = json.dumps(payload)
                response = requests.post(app_view.DB_HOST + app_view.update_job_api, data=payload)

                if response.status_code == 200:
                    messages.success(request, f'Your Job File {file_uploaded.name} has been uploaded to the Job-{job_id} Successfully!')
                else:
                    messages.warning(request, f"Your Job File couldn't be uploaded, Please try again!")

            else:
                # DB call :: removeJob
                # Job entry has been already initiated in the DB in  order to get the job id.
                # Now that entry is reverted here, since the job json is invalid.
                payload = {'job_id': job_id}
                payload = json.dumps(payload)

                response = requests.post(app_view.DB_HOST + app_view.remove_job_api, data=payload)

    return redirect('jobs')


def refresh_job(request, job_id, process):
    if 'running' in process.lower():
        p_id = int(process[-1])
        process_status = job_executor.query_process(p_id)
        # DB call :: updateJob
        payload = {'job_id': job_id, 'jobs_process': process_status}
        payload = json.dumps(payload)
        response = requests.post(app_view.DB_HOST + app_view.update_job_api, data=payload)

    return redirect('jobs')


def remove_job(request):
    if request.method == 'POST':
        username = request.user.username
        job_id = request.POST['job_id']

        # DB call :: removeJob
        payload = {'job_id': job_id}
        payload = json.dumps(payload)

        response = requests.post(app_view.DB_HOST + app_view.remove_job_api, data=payload)
        if response.status_code == 200:
            messages.success(request, f'Your Job-{job_id}] has been removed Successfully!')
            data_storage.delete_job(username, job_id)
        else:
            messages.warning(request, f"Your Job-{job_id}]  couldn't be removed, Please try again!")

    return redirect('jobs')

def get_data_sets_from_fs(username):
    """
    :param username:
    :return: Dictionary -> {data_set_id: ("dataset_url", [dataset_files])}
    """
    """data_sets_list = data_storage.list_datasets(username)"""
    payload = {"username": username}
    payload = json.dumps(payload)
    response = requests.post(app_view.DB_HOST + app_view.getFSDatasets_api, data=payload)
    data_sets = (response.json())["datasets"]

    return data_sets


def view_dataset_file(request, data_set_id, file_name):
    #Note: Users should be allowed to view, only the files which are uploaded by them.
    username = request.user.username
    try:
        payload = {"username": username, "data_set_id": data_set_id, "file_name": file_name}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        dataset_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSDataset_api, data=payload)
        response = HttpResponse(dataset_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "inline; filename=" + file_name
        return response
    except Exception:
        raise Http404


def download_dataset_file(request, data_set_id, file_name):
    # Note: Users should be allowed to download, only the files which are uploaded by them.
    username = request.user.username
    try:
        payload = {"username": username, "data_set_id": data_set_id, "file_name": file_name}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        dataset_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSDataset_api, data=payload)
        response = HttpResponse(dataset_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "attachment; filename=" + file_name
        return response
    except Exception:
        raise Http404


def get_jobs_from_fs(username):
    """
    :param username:
    :return:  Dictionary -> {job_id: "job_url"}
    """
    payload = {"username": username}
    payload = json.dumps(payload)
    response = requests.post(app_view.DB_HOST + app_view.getFSJobs_api, data=payload)
    jobs = (response.json())["jobs"]

    return jobs

def view_job_file(request, job_id, file_name):
    # Note: Users should be allowed to view, only the files which are uploaded by them.
    username = request.user.username
    try:
        payload = {"username": username, "job_id": job_id, "file_name": file_name}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        job_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSJob_api, data=payload)
        response = HttpResponse(job_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "inline; filename=" + file_name
        return response
    except Exception:
        raise Http404

def download_job_file(request, job_id, file_name):
    # Note: Users should be allowed to download, only the files which are uploaded by them.
    username = request.user.username
    try:
        payload = {"username": username, "job_id": job_id, "file_name": file_name}
        payload = json.dumps(payload)
        # File Response[File as IO object] will be received.
        job_file_content = requests.post(app_view.DB_HOST + app_view.getFileFromFSJob_api, data=payload)
        response = HttpResponse(job_file_content, content_type=mimetypes.guess_type(file_name)[0])
        response['Content-Disposition'] = "attachment; filename=" + f'job_{job_id}.json'
        return response
    except Exception:
        raise Http404

