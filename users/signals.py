#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    MagikEye Signals for invoking certain actions based on changes Occurring in the DataBase models.
"""

__author__ = "karthik"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ----------------------------------------------------------------------------------------------------------------------

from django.db.models.signals import post_save, post_delete
from django.contrib.auth.models import User
from django.dispatch import receiver
from .models import Profile

from connecto import views as app_view
from users import views as user_views
import json, requests

@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    """
    :Author: karthik
    :Description:Creates a Profile whenever a new User instance have got created in User model.
    """
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    """
    :Author: karthik
    :Description: Created Profile of a user instance will be saved.
    """
    instance.profile.save()


@receiver(post_save, sender=User)
def create_file_system_for_user(sender, instance, **kwargs):
    """
    :Author: karthik
    :Description: Creates a filesystem whenever a new User instance have got created in User model.
    """
    username = instance.username
    """
    if username not in user_views.data_storage.list_users():
          user_views.data_storage.create_user(username)
    """
    payload = {"username": username}
    payload = json.dumps(payload)
    requests.post(app_view.DB_HOST + app_view.createFSUser_api, data=payload)

@receiver(post_delete, sender=User)
def remove_data_of_deleted_user(sender, instance, **kwargs):
    """
    :Author: karthik
    :Description: Removes all Datasets,jobs from DataBase and deletes entire filesystem, Whenever a user gets deleted.
    """
    username = instance.username
    payload = {'username': username}
    payload = json.dumps(payload)

    # Remove Data Sets from DB
    data_sets_remove_response = requests.post(app_view.DB_HOST + app_view.remove_all_data_sets_api, data=payload)
    assert data_sets_remove_response.status_code == 200

    # Remove Jobs from DB
    jobs_remove_response = requests.post(app_view.DB_HOST + app_view.remove_all_jobs_api, data=payload)
    assert jobs_remove_response.status_code == 200

    # Remove all data for user from File System
    """ 
    if username in user_views.data_storage.list_users():
         user_views.data_storage.delete_user(username)
     """
    payload = {"username": username}
    payload = json.dumps(payload)
    requests.post(app_view.DB_HOST + app_view.deleteFSUser_api, data=payload)



