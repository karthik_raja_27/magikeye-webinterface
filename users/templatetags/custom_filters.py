from django import template

register = template.Library()
@register.filter()
def hash(id):
    hash = hashlib.md5()
    hash.update("%s:%s" % (id, settings.ADMIN_HASH_SECRET))
    return hash.hexdigest().upper()