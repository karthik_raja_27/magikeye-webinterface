from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required

from users import views as user_views
import os.path
import time
import requests
import json

DB_HOST = "http://127.0.0.1:8002/"
all_datasets = 'api/datasets/'
all_jobs = 'api/jobs/'
create_data_set_api = 'api/createDataSet/'
update_data_set_api = 'api/updateDataSet/'
remove_data_set_api = 'api/removeDataSet/'
remove_all_data_sets_api = 'api/removeAlldatasets/'

create_job_api = 'api/createJob/'
update_job_api = 'api/updateJob/'
remove_job_api = 'api/removeJob/'
remove_all_jobs_api = 'api/removeAlljobs/'

# FS_API'S
getFSUsers_api = 'api/getFSUsers/'
createFSUser_api = 'api/createFSUser/'

createFSDataset_api = 'api/createFSDataset/'
getFSDatasets_api = 'api/getFSDatasets/'
addFileToFSDataset_api = 'api/addFileToFSDataset/'
listFSDatasetFiles_api = 'api/listFSDatasetFiles/'
getFileFromFSDataset_api = 'api/getFileFromFSDataset/'
removeFSDataset_api = 'api/removeFSDataset/'


createFSJob_api = 'api/createFSJob/'
getFSJobs_api = 'api/getFSJobs/'
getFileFromFSJob_api = 'api/getFileFromFSJob/'
removeFSJob_api = 'api/removeFSJob/'

deleteFSUser_api = 'api/deleteFSUser/'

@login_required
def home(request):
    username = request.user.username
    All_Datasets = requests.get(DB_HOST + all_datasets).json()
    Datasets = []
    for Dataset in All_Datasets:
        if Dataset['dataset_user'] == username:
            date_created = Dataset['dataset_date_created'][:10]
            time = Dataset['dataset_date_created'][11:19]
            """no_of_files = len(user_views.data_storage.list_dataset_files(username, Dataset['id']))"""
            payload = {"username": username, "data_set_id": Dataset['id']}
            payload = json.dumps(payload)
            response = requests.post(DB_HOST + listFSDatasetFiles_api, data=payload)
            data_set_files = (response.json())["data_set_files"]
            Dataset.update({'dataset_date_created': date_created+'   '+time, 'no_of_files': len(data_set_files)})
            Datasets.append(Dataset)

    return render(request, 'home.html', {'title': 'MagikEye - Home', 'data_sets': Datasets})

@login_required
def jobs(request):
    username = request.user.username
    AllJobs = requests.get(DB_HOST + all_jobs).json()
    Jobs = []
    for Job in AllJobs:
        if Job['jobs_user'] == username:
            date_created = Job['jobs_date_created'][:10]
            time = Job['jobs_date_created'][11:19]
            Job.update({'jobs_date_created': date_created+'   '+time})
            Jobs.append(Job)

    return render(request, 'jobs.html', {'title': 'MagiEye', 'jobs': Jobs})

def dataset_details(request, data_set_id):
    username = request.user.username
    data_sets_from_fs = user_views.get_data_sets_from_fs(username)
    data_set_url = data_sets_from_fs[data_set_id][0]
    data_set_files =  data_sets_from_fs[data_set_id][1]
    data_set_file_details = []
    for data_set_file in data_set_files:
        file_path = data_set_url + "/" + data_set_file
        file_uploaded_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(os.path.getmtime(file_path)))
        current_file_detail = dict()
        current_file_detail['name'] = data_set_file
        current_file_detail['uploaded_time'] = file_uploaded_time
        current_file_detail['file_path'] = file_path
        data_set_file_details.append(current_file_detail)

    content = {'title': 'MagiEye', 'data_set_id': data_set_id,
               'data_set_url': data_set_url, 'data_set_file_details': data_set_file_details}

    return render(request, 'dataset_details.html', content)

def job_details(request, job_id):
    username = request.user.username
    jobs_from_fs = user_views.get_jobs_from_fs(username)

    job_details = []
    for jobid, job_url in jobs_from_fs.items():
        if jobid == job_id:
            file_path = job_url + "/" + 'job.json'
            file_uploaded_time = time.strftime('%Y-%m-%d   %H:%M:%S', time.gmtime(os.path.getmtime(file_path)))
            current_file_detail = dict()
            current_file_detail['name'] = 'job.json'
            current_file_detail['uploaded_time'] = file_uploaded_time
            current_file_detail['file_path'] = file_path
            job_details.append(current_file_detail)

    content = {'title': 'MagiEye', 'job_id': job_id,
               'job_url': job_url, 'job_details': job_details}

    return render(request, 'job_details.html', content)


@login_required
def admin_view(request):
    users = User.objects.all()
    return render(request, 'admin_view.html', {'title': 'MagikEye - Admin View', 'customers': users})

@login_required
def admin_as_user_login(request, user_name):
    user_to_be_logged_in_as = User.objects.get(username=user_name)

    if request.user.is_superuser:
        login(request, user_to_be_logged_in_as)
        return redirect("home")

    else:
        return redirect("login")

